package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class BaseControllerTest {

	@Test
	public void testWelcome() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcome(map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - "));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testWelcomeName() throws Exception {
        	ModelMap map = mock(ModelMap.class);
		String result = new BaseController().welcomeName("test", map);
		verify(map).addAttribute(eq("message"), startsWith("Welcome - test -"));
		verify(map).addAttribute(eq("counter"), anyInt());
	}

	@Test
	public void testAbout(){

	About abt = new About();
	assertTrue(abt.desc().contains( "was copied from" ));

	}

	@Test
	public void testCalcmul(){

	Calcmul calc = new Calcmul();
	assertEquals(calc.mul(),18);

	}

	@Test
	public void testCalculator(){
	
	Calculator calc = new Calculator();
	assertEquals(calc.add(),9);
	
	}

	@Test
	public void testCnt(){

	Cnt cnt = new Cnt();
	assertEquals(cnt.d(6,2),3);
	assertEquals(cnt.d(4,0),Integer.MAX_VALUE);
	}
}
